const inp = document.getElementById('fileInp');
const btn = document.getElementById('uploadBtn');
const filesContent = document.querySelector('.filesContent');
const url = 'http://192.168.0.107';

function uploadFiles(x,step,files) {

    for (let i = x; i < x + step; i++) {
        
        if (!files[i]) {
            return
        }
        
        const xhr = new XMLHttpRequest();
        
        const formData = new FormData();
        formData.append('files', files[i]);
        
        xhr.open("POST", url);
        xhr.upload.addEventListener("progress", function (e) {
            if (e.lengthComputable) {
                let complete = (e.loaded / e.total * 100 | 0);
                document.querySelector(`span[data-name="${files[i].name}"]`).innerHTML = complete + '%';
                document.querySelector(`div[data-name="${files[i].name}"]`).style.width = complete + '%';
            }
        });
        xhr.send(formData);
        xhr.onreadystatechange = function(e) {
            if (xhr.readyState === 4 && JSON.parse(xhr.response).status === true) {
                x ++;
                if (x % step === 0) {
                    uploadFiles(x,step,files);
                }
            }
        }
    }
}


function drawFiles(files) {

    for (let i = 0; i < files.length; i++) {
        
        let type = files[i].type.split('/').pop().toLowerCase();
        let imgSrc;
        if(type === "jpeg" || type === "jpg" || type === "png" || type === "gif") {
            imgSrc = type
        }

        filesContent.innerHTML += 
        `
        <div class="fileBlock">
            <h3>${files[i].name}</h3>
            <div class="fileImg">
                ${imgSrc ? `<img src='' data-name="${files[i].name}">` :  `<i class="fas fa-file icon"></i>`}
            </div>
            <div class="progressBar">
                <div class='progressPercentage' data-name="${files[i].name}"></div>
                <span class="persent" data-name="${files[i].name}">0%</span>
            </div>
        </div>
    
        `
        if (imgSrc) {
            const reader = new FileReader();
            reader.onload = function(e) {
                document.querySelector(`img[data-name="${files[i].name}"]`).src = e.currentTarget.result;
            }
            reader.readAsDataURL(files[i]);
        }
    }
}



function drop(e) {
    
    e.preventDefault();

    let dropedFiles = [];

    if (e.dataTransfer.items) {
        for (let i = 0; i < e.dataTransfer.items.length; i++) {
            if (e.dataTransfer.items[i].kind === 'file') {
                dropedFiles.push(e.dataTransfer.items[i].getAsFile());
            }
        }

        btn.style.display = 'block';
        filesContent.innerHTML = '';
        drawFiles(dropedFiles);

        btn.addEventListener('click', uploadWithParametrs,{once:true});
        btn.param = dropedFiles;
    } 
}


function drag(e) {
    e.preventDefault();
}


function uploadWithParametrs(e){
    let x = 0;
    const step = 3;
    uploadFiles(x,step,e.currentTarget.param);
}

inp.addEventListener('change', function(e) {
    const files = inp.files;
    btn.style.display = 'block';
    
    filesContent.innerHTML = '';
    drawFiles(files);
    
    btn.addEventListener('click', uploadWithParametrs,{once:true});
    btn.param = files;
})




